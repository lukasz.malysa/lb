<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 *
 * Simple php object/array configuration
 * Object contains 2 properties:
 * 1. strategy - array with class of strategy and arg used in constructor
 * 2. hosts - array of hosts with their settings
 *
 */
return (object) array(
    'strategy' => array(
        'class' => '\Lb\Strategy\RoundRobin',
        'arg' => new \Lb\Storage\File(__DIR__.'/data/state.txt')
    ),
    /*'strategy' => array(
        'class' => '\Lb\Strategy\LowLoad',
        'arg' => 0.75
    ),*/
    /*'strategy' => array(
        'class' => '\Lb\Strategy\LowestLoad'
    ),*/
    /*'strategy' => array(
        'class' => '\Lb\Strategy\Random'
    ),*/
    'hosts' => array(
        'web01.ez.local' => array(
            'class' => 'Lb\Host\Instance\Configured',
            'load' => 0.43
        ),
        'web02.ez.local' => array(
            'class' => 'Lb\Host\Instance\Configured',
            'load' => 0.02
        ),
        'web03.ez.local' => array(
            'class' => 'Lb\Host\Instance\Configured',
            'load' => 0.13
        ),
        'web04.ez.local' => array(
            'class' => 'Lb\Host\Instance\Configured',
            'load' => 0.73
        )

    )
);