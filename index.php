<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

require(__DIR__.'/vendor/autoload.php');

use Lb\Request;
use Lb\Host\Pool;

$config = require('config.php');

$request = Request::createFromGlobals();

$balancer = new \Lb\LoadBalancer(
    new Pool($config),
    new $config->strategy['class'](
        isset($config->strategy['arg']) ? $config->strategy['arg'] : null
    )
);
$balancer->handleRequest($request);