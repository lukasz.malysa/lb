<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

namespace Lb\Host\Instance;

/**
 * Class AbstractInstance
 *
 * Abstract class of host instance
 * Contains implementation of methods required by all subclasses
 *
 * @package Lb\Host\Instance
 */
abstract class AbstractInstance
{
    protected $hostName = null;

    public function __construct(string $hostName, $data)
    {
        $this->hostName = $hostName;
        $this->configure($data);
    }

    protected function configure($data) {
        foreach ($data as $configkey => $configvalue) {
            $this->$configkey = $configvalue;
        }
    }

    public function getHostName() : string {
        return $this->hostName;
    }

    public function handleRequest(\Lb\Request $request) {
        echo "\nThis is from ". $this->hostName. " current load is ". $this->getLoad()."\n";
    }

}