<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

namespace Lb\Host\Instance;

/**
 * Class Configured
 *
 * Class of host instance where load can be set in configuration
 *
 * @package Lb\Host\Instance
 */
class Configured extends AbstractInstance implements InstanceInterface
{
    public function getLoad(): float
    {
        if (isset($this->load)) {
            return $this->load;
        }
        throw new \Exception('Load not configured for '.$this->getHostName());
    }
}