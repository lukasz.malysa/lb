<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

namespace Lb\Host\Instance;

interface InstanceInterface
{
    public function getLoad() : float;

    public function getHostName() : string ;

    public function handleRequest(\Lb\Request $request);

}