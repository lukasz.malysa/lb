<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

namespace Lb\Host\Instance;

/**
 * Class Random
 *
 * Host instance class where load is random
 *
 * @package Lb\Host\Instance
 */
class Random extends AbstractInstance implements InstanceInterface
{
    public function getLoad(): float
    {
        return rand(0,100)/100;
    }
}