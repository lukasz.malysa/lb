<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

namespace Lb\Host;

/**
 * Class Pool
 *
 * This class represents a set of hosts accessed by load balancer
 *
 * @package Lb\Host
 */
class Pool implements \Iterator, \Countable, \ArrayAccess
{
    protected $position = 0;

    protected $hosts = array();

    protected $config = array();

    public function __construct($config) {
        $this->config = $config;
        $this->position = 0;
        $this->initHosts();
    }

    public function offsetSet($offset, $value) {
        return false;
    }

    public function offsetExists($offset) {
        return isset($this->hosts[$offset]);
    }

    public function offsetUnset($offset) {
        return false;
    }

    public function offsetGet($offset) {
        return isset($this->hosts[$offset]) ? $this->hosts[$offset] : null;
    }

    public function count() {
        return count($this->hosts);
    }

    public function rewind() {
        $this->position = 0;
    }

    public function current() {
        return $this->hosts[$this->position];
    }

    public function key() {
        return $this->position;
    }

    public function next() {
        ++$this->position;
    }

    public function valid() {
        return isset($this->hosts[$this->position]);
    }

    protected function initHosts() {
        if (!isset($this->config->hosts)) {
            return;
        }

        foreach ($this->config->hosts as $hostname => $data) {
            $this->hosts[] = new $data['class']($hostname, $data);
        }
    }

}