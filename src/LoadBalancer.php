<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

namespace Lb;

use \Lb\Strategy\LoadBalancerStrategyInterface;
use \Lb\Host\Pool;

class LoadBalancer implements LoadBalancerInterface
{
    protected $strategy = null;
    protected $pool = null;

    public function __construct(Pool $pool, LoadBalancerStrategyInterface $strategy) {
        $this->strategy = $strategy;
        $this->pool = $pool;
    }

    public function handleRequest(Request $request) {

        $this->strategy->setPool($this->pool);

        $chosenHost = $this->strategy->chooseHostFromPool();

        $chosenHost->handleRequest($request);
    }


}