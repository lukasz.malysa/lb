<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

namespace Lb;

use \Lb\Strategy\LoadBalancerStrategyInterface;
use \Lb\Host\Pool;

interface LoadBalancerInterface
{
    public function __construct(Pool $pool, LoadBalancerStrategyInterface $strategy);

    public function handleRequest(Request $request);

}