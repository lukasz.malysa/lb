<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

namespace Lb\Storage;

use \Lb\Host\Instance\InstanceInterface;

/**
 * Class File
 *
 * Storing state in file
 *
 * @package Lb\Storage
 */
class File implements StorageInterface
{
    protected $fileName = '';

    public function __construct($fileName)
    {
        $this->fileName = $fileName;
    }

    public function setStoredState(InstanceInterface $chosenHost) {
        file_put_contents($this->fileName, $chosenHost->getHostName());
    }

    public function getStoredState() : string {
        return file_get_contents($this->fileName);
    }
}