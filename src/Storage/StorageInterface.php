<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

namespace Lb\Storage;

use \Lb\Host\Instance\InstanceInterface;

/**
 * Interface StorageInterface
 *
 * This is interface for setting current state
 * and getting previous stored state
 *
 * @package Lb\Storage
 */
interface StorageInterface
{
    public function setStoredState(InstanceInterface $chosenHost);

    public function getStoredState() : string;

}