<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

namespace Lb\Strategy;

use \Symfony\Component\HttpFoundation\Request;
use \Lb\Host\Pool;

/**
 * Class AbstractStrategy
 *
 * Each object of subclass of AbstractStrategy
 * must be set externally with Pool object
 * Strategies also do handleRequest on choosen host in handleRequest method
 *
 * @package Lb\Strategy
 */
abstract class AbstractStrategy implements LoadBalancerStrategyInterface
{
    protected $pool = array();

    protected $loads = array();

    public function setPool(Pool $pool) {
        $this->pool = $pool;
    }

    public function handleRequest(Request $request) {
        $this->chooseHostFromPool()->handleRequest($request);
    }

}