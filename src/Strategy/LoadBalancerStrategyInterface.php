<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

namespace Lb\Strategy;

use Lb\Host\Pool;
use \Lb\Host\Instance\InstanceInterface;

/**
 * Interface LoadBalancerStrategyInterface
 *
 * Each class implementing LoadBalancerStrategyInterface
 * is responsible of choosing appropriate host
 *
 * @package Lb\Strategy
 */
interface LoadBalancerStrategyInterface
{
    public function chooseHostFromPool() : InstanceInterface;
}