<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

namespace Lb\Strategy;

/**
 * Class LowLoad
 *
 * The strategy chooses first host with load lower than configured
 * If no such host found, it chooses the lowest from all
 *
 * @package Lb\Strategy
 */
class LowLoad extends LowestLoad implements LoadBalancerStrategyInterface
{

    protected $border = 0;

    public function __construct($border = 0)
    {
        $this->border = $border;
    }

    public function chooseHostFromPool() : \Lb\Host\Instance\InstanceInterface {
        foreach ($this->pool as $host) {
            if ($host->getLoad() < $this->border) {
                return $host;
            }
        }

        return parent::chooseHostFromPool();

    }


}