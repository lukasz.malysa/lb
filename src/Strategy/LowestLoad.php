<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

namespace Lb\Strategy;

/**
 * Class LowestLoad
 *
 * The strategy chooses host with lowest load
 *
 * @package Lb\Strategy
 */
class LowestLoad extends AbstractStrategy implements LoadBalancerStrategyInterface
{

    public function chooseHostFromPool() : \Lb\Host\Instance\InstanceInterface {
        foreach ($this->pool as $host) {
            $this->loads[] = $host->getLoad();
        }

        $minLoad = min($this->loads);
        $key = array_search($minLoad, $this->loads);
        return $this->pool[$key];

    }


}