<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

namespace Lb\Strategy;

/**
 * Class Random
 *
 * Strategy chooses randomly host from a pool
 *
 * @package Lb\Strategy
 */
class Random extends AbstractStrategy implements LoadBalancerStrategyInterface
{

    public function chooseHostFromPool() : \Lb\Host\Instance\InstanceInterface {
        $key = rand(0, count($this->pool) - 1);
        return $this->pool[$key];

    }


}