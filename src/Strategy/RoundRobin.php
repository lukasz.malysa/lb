<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

namespace Lb\Strategy;

/**
 * Class RoundRobin
 *
 * Strategy chooses the host in Round Robin way
 * It use storage to determine last state
 *
 * @package Lb\Strategy
 */
class RoundRobin extends AbstractStrategy implements LoadBalancerStrategyInterface
{
    protected $map = array();

    protected $storage = null;

    public function __construct($storage)
    {
        $this->storage = $storage;
    }

    public function chooseHostFromPool() : \Lb\Host\Instance\InstanceInterface {
        foreach ($this->pool as $host) {
            $this->map[] = $host->getHostName();
        }

        $last = $this->storage->getStoredState();
        $lastIndex = array_search($last, $this->map);
        if ($lastIndex === false) {
            $newIndex = 0;
        } else {
            $newIndex = $lastIndex + 1;
        }


        if ($newIndex > count($this->pool) - 1) {
            $newIndex = 0;
        }

        $chosen = $this->pool[$newIndex];
        $this->storage->setStoredState($chosen);

        return $chosen;
    }


}