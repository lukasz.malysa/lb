<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

namespace Lb\Tests\Strategy;

use Lb\Strategy\LowestLoad;
use Lb\Host\Pool;
use PHPUnit\Framework\TestCase;

class AbstractStrategy extends TestCase
{

    protected function getChosenHostName($strategy, $config) {
        $strategy->setPool( new Pool($config));
        $chosen = $strategy->chooseHostFromPool();
        return $chosen->getHostName();
    }

    public function testFake()
    {
        //I need this one test function in the class
        //because I need one abstract class
        $this->assertTrue(true);
    }

    public function getConfigDescendingBelowBorder()
    {
        return (object) array(
            'hosts' => array(
                'web01.ez.local' => array(
                    'class' => 'Lb\Host\Instance\Configured',
                    'load' => 0.43
                ),
                'web02.ez.local' => array(
                    'class' => 'Lb\Host\Instance\Configured',
                    'load' => 0.13
                ),
                'web03.ez.local' => array(
                    'class' => 'Lb\Host\Instance\Configured',
                    'load' => 0.02
                )

            )
        );
    }

    public function getConfigDescendingAboveBorder()
    {
        return (object) array(
            'hosts' => array(
                'web01.ez.local' => array(
                    'class' => 'Lb\Host\Instance\Configured',
                    'load' => 1
                ),
                'web02.ez.local' => array(
                    'class' => 'Lb\Host\Instance\Configured',
                    'load' => 0.8
                ),
                'web03.ez.local' => array(
                    'class' => 'Lb\Host\Instance\Configured',
                    'load' => 0.78
                )

            )
        );
    }

    public function getConfigAscendingBelowBorder()
    {
        return (object) array(
            'hosts' => array(
                'web01.ez.local' => array(
                    'class' => 'Lb\Host\Instance\Configured',
                    'load' => 0.1
                ),
                'web02.ez.local' => array(
                    'class' => 'Lb\Host\Instance\Configured',
                    'load' => 0.2
                ),
                'web03.ez.local' => array(
                    'class' => 'Lb\Host\Instance\Configured',
                    'load' => 0.3
                )

            )
        );

    }

    public function getConfigAscendingAboveBorder()
    {
        return (object) array(
            'hosts' => array(
                'web01.ez.local' => array(
                    'class' => 'Lb\Host\Instance\Configured',
                    'load' => 0.8
                ),
                'web02.ez.local' => array(
                    'class' => 'Lb\Host\Instance\Configured',
                    'load' => 0.9
                ),
                'web03.ez.local' => array(
                    'class' => 'Lb\Host\Instance\Configured',
                    'load' => 1
                )

            )
        );

    }
}