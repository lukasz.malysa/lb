<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

namespace Lb\Tests\Strategy;

use Lb\Strategy\LowLoad;

class LowLoadTest extends AbstractStrategy
{
    public function testCases()
    {
        $border = 0.75;
        $this->assertEquals(
            $this->getChosenHostName(new LowLoad($border), $this->getConfigAscendingAboveBorder()),
            'web01.ez.local'
        );

        $this->assertEquals(
            $this->getChosenHostName(new LowLoad($border), $this->getConfigAscendingBelowBorder()),
            'web01.ez.local'
        );

        $this->assertEquals(
            $this->getChosenHostName(new LowLoad($border), $this->getConfigDescendingAboveBorder()),
            'web03.ez.local'
        );

        $this->assertEquals(
            $this->getChosenHostName(new LowLoad($border), $this->getConfigDescendingBelowBorder()),
            'web01.ez.local'
        );
    }
}