<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

namespace Lb\Tests\Strategy;

use Lb\Strategy\LowestLoad;

class LowestLoadTest extends AbstractStrategy
{

    public function testCases()
    {
        $this->assertEquals(
            $this->getChosenHostName(new LowestLoad(), $this->getConfigAscendingAboveBorder()),
            'web01.ez.local'
        );

        $this->assertEquals(
            $this->getChosenHostName(new LowestLoad(), $this->getConfigAscendingBelowBorder()),
            'web01.ez.local'
        );

        $this->assertEquals(
            $this->getChosenHostName(new LowestLoad(), $this->getConfigDescendingAboveBorder()),
            'web03.ez.local'
        );

        $this->assertEquals(
            $this->getChosenHostName(new LowestLoad(), $this->getConfigDescendingBelowBorder()),
            'web03.ez.local'
        );
    }


}