<?php
/**
 * Created by lmalysa <lukasz.malysa@gmail.com>
 */

namespace Lb\Tests\Strategy;

use Lb\Strategy\RoundRobin;

class RoundRobinTest extends AbstractStrategy
{

    /**
     * Test first time
     */
    public function testOne()
    {
        $fileName = __DIR__.'/../../data/tests/test.txt';
        file_put_contents($fileName, '');

        $storage = new \Lb\Storage\File($fileName);

        $this->assertEquals(
            $this->getChosenHostName(new RoundRobin($storage), $this->getConfigAscendingAboveBorder()),
            'web01.ez.local'
        );
    }

    /**
     * Test transition from host1 to host2
     */
    public function testTwo()
    {
        $fileName = __DIR__.'/../../data/tests/test.txt';
        file_put_contents($fileName, 'web01.ez.local');

        $storage = new \Lb\Storage\File($fileName);

        $this->assertEquals(
            $this->getChosenHostName(new RoundRobin($storage), $this->getConfigAscendingAboveBorder()),
            'web02.ez.local'
        );
    }

    /**
     * Test transition from host2 to host3
     */
    public function testThree()
    {
        $fileName = __DIR__.'/../../data/tests/test.txt';
        file_put_contents($fileName, 'web02.ez.local');

        $storage = new \Lb\Storage\File($fileName);

        $this->assertEquals(
            $this->getChosenHostName(new RoundRobin($storage), $this->getConfigAscendingAboveBorder()),
            'web03.ez.local'
        );
    }

    /**
     * Test transition from the last host, again to first
     */
    public function testFour()
    {
        $fileName = __DIR__.'/../../data/tests/test.txt';
        file_put_contents($fileName, 'web03.ez.local');

        $storage = new \Lb\Storage\File($fileName);

        $this->assertEquals(
            $this->getChosenHostName(new RoundRobin($storage), $this->getConfigAscendingAboveBorder()),
            'web01.ez.local'
        );
    }


}